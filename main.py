import sys
import mysql.connector
import search, create, modify
import argparse

operations_police = {"Search": search.construct_query, "Create": create.build_accident, "Modify": modify.modify_accident}
operations_analyst = {"Search": search.construct_query}

def main():
    # TODO: Add users as mentioned in project description? Just means allowing creation/modification or not
    cnx = mysql.connector.connect(user='root', password='123456', host='localhost', database='local2')
    user = input("The available users are Police and Analyst. Please choose your user: ").lower()
    while user not in ["police", "analyst"]:
        user = input("Invalid user. The available users are Police and Analyst. Please choose your user: ").lower()
    operations = operations_police if user == "police" else operations_analyst; 
    print("What would you like to do? The supported operations are: " + " ".join(operations.keys()) + ".")
    cursor = cnx.cursor(dictionary=True)
    while(True):
        op = input("Please enter your desired operation, or 'exit' to quit: ").title()
        while op not in operations:
            if op.lower() == "exit":
                cursor.close()
                cnx.close()
                exit(0)
            print("Operation not found. The supported operations are: " + " ".join(operations.keys()) + ".")
            op = input("Please enter your desired operation, or 'exit' to quit: ").title()
        operations[op](cnx, cursor)
        cursor.reset()


if __name__ == "__main__":
    main()