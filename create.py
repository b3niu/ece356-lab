import datetime
import beautifultable
import constants
import mysql.connector

def build_accident(cnx, cursor):
    accidents = {}
    print("Accident Details")
    for key, value in constants.filters["Accident Details"].items():
        if value == "date" or value == "time":
            continue
        accidents[value] = input("Please enter the " + key + ": ")

    try:
        year = int(input("Please enter the year (YYYY): "))
        month = int(input("Please enter the month (MM): "))
        day = int(input("Please enter the day (DD): "))
        hour = int(input("Please enter the hour (HH): "))
        minute = int(input("Please enter the minute (MM): "))
        accidents["date"] = datetime.date(year, month, day)
        accidents["time"] = datetime.time(hour, minute)
    except BaseException:
        print("Invalid date or time entered, cancelling create")
        return

    accidentID = accidents["accidentID"]

    roadInfo = {"accidentID": accidentID}
    print("Road Information")
    for key, value in constants.filters["Road"].items():
        roadInfo[value] = input("Please enter the " + key + ": ")

    secondRoad = input("Is there a second road in this accident? (y/n): ").lower()
    while secondRoad not in ["y", "yes", "n", "no"]:
        secondRoad = input("Invalid response. Is there a second road in this accident? (y/n): ").lower()
    if secondRoad == "y" or secondRoad == "yes":
        intersection = {"accidentID": accidentID}
        print("Intersection")
        for key, value in constants.filters["Intersection"].items():
            intersection[value] = input("Please enter the " + key + ": ")

    print("Accident Conditions")
    conditions = {"accidentID": accidentID}
    for key, value in constants.filters["Conditions"].items():
        if "Conditions" not in key:
            key = key + " Conditions"
        conditions[value] = input("Please enter the " + key + ": ")

    print("Accident Location")
    location = {"accidentID": accidentID}
    for key, value in constants.filters["Location"].items():
        location[value] = input("Please enter the " + key + ": ")

    casualtyCur = 1
    vehicles = []
    people = []
    drivers = []
    casualties = []
    for vehicleNum in range(1, int(accidents["num_vehicles"])+1):
        newVehicle = {
            "accidentID": accidentID,
            "vehicleRef": vehicleNum
        }
        print("For vehicle " + str(vehicleNum) + ":")
        for key, value in constants.filters["Vehicle"].items():
            if value == "vehicleRef":
                continue
            newVehicle[value] = input("Please enter the " + key + ": ")

        newPerson = {
            "accidentID": accidentID,
            "vehicleRef": vehicleNum
        }
        for key, value in constants.create["Person"].items():
            if value == "casualtyRef":
                continue
            newPerson[value] = input("Please enter the driver " + key + ": ")

        newDriver = {
            "accidentID": accidentID,
            "vehicleRef": vehicleNum
        }
        for key, value in constants.create["Driver"].items():
            newDriver[value] = input("Please enter the driver " + key + ": ")

        casualty = input("Is the driver a casualty? (y/n): ").lower()
        while casualty not in ["y", "yes", "n", "no"]:
            casualty = input("Invalid response. Is the driver a casualty? (y/n): ").lower()
        if casualty == "y" or casualty == "yes":
            newPerson["casualtyRef"] = casualtyCur
            newDriver["casualtyRef"] = casualtyCur
            newCasualty = {
                "accidentID": accidentID,
                "vehicleRef": vehicleNum,
                "casualtyRef": casualtyCur,
                "casualtyClass": 1
            }
            casualtyCur += 1
            for key, value in constants.create["Casualties"].items():
                if value == "casualtyClass":
                    continue
                newCasualty[value] = input("Please enter the " + key + ": ")

            casualties.append(newCasualty)
        else:
            newPerson["casualtyRef"] = -1
            newDriver["casualtyRef"] = -1

        vehicles.append(newVehicle)
        people.append(newPerson)
        drivers.append(newDriver)


    casualtyCount = int(accidents["num_casualties"])
    for casualtyNum in range(casualtyCur, casualtyCount+1):
        newPerson = {
            "accidentID": accidentID,
            "casualtyRef": casualtyNum
        }
        newCasualty = {
            "accidentID": accidentID,
            "casualtyRef": casualtyNum
        }
        print("For casualty " + str(casualtyNum) + ": ")
        newCasualty["vehicleRef"] = input("Please enter the reference vehicle:")
        newPerson["vehicleRef"] = newCasualty["vehicleRef"]
        for key, value in constants.create["Person"].items():
            if value == "casualtyRef":
                continue
            newPerson[value] = input("Please enter the " + key + ": ")
        for key, value in constants.create["Casualties"].items():
            newCasualty[value] = input("Please enter the " + key + ": ")
        casualties.append(newCasualty)

    try:
        query = "insert into Accidents values(%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        data = (
            accidents["accidentID"],
            int(accidents["accidentSeverity"]),
            int(accidents["num_vehicles"]),
            int(accidents["num_casualties"]),
            accidents["date"],
            int(accidents["dayOfWeek"]),
            accidents["time"],
            int(accidents["policeForce"]),
            int(accidents["policeAttendance"]),
        )
        cursor.execute(query, data)

        query = "insert into RoadInfo values(%s,%s,%s,%s,%s,%s,%s)"
        data = (
            roadInfo["accidentID"],
            int(roadInfo["roadType"]),
            int(roadInfo["firstRoadClass"]),
            int(roadInfo["firstRoadNumber"]),
            roadInfo["speedLimit"],
            int(roadInfo["pedCrossingHuman"]),
            int(roadInfo["pedCrossingPhysical"])
        )
        cursor.execute(query, data)

        if secondRoad == "y" or secondRoad == "yes":
            query = "insert into Intersection values(%s,%s,%s,%s,%s,%s)"
            data = (
                intersection["accidentID"],
                int(intersection["secondRoadClass"]),
                int(intersection["secondRoadNumber"]),
                int(intersection["junctionDetail"]),
                int(intersection["junctionControl"]),
                int(intersection["junctionLocation"]),
            )
            cursor.execute(query, data)

        query = "insert into AccidentConditions values(%s,%s,%s,%s,%s,%s)"
        data = (
            conditions["accidentID"],
            int(conditions["lightConditions"]),
            int(conditions["weatherConditions"]),
            int(conditions["roadSurfaceConditions"]),
            int(conditions["specialConditions"]),
            int(conditions["carriagewayHazards"]),
        )
        cursor.execute(query, data)

        query = "insert into LocationData values(%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        data = (
            location["accidentID"],
            int(location["locationEasting"]),
            int(location["locationNorthing"]),
            float(location["locationLatitude"]),
            float(location["locationLongitude"]),
            int(location["localAuthorityDistrict"]),
            location["localAuthorityHighway"],
            int(location["urbanOrRural"]),
            location["LSOA"],
        )
        cursor.execute(query, data)

        for vehicle in vehicles:
            query = "insert into Vehicle values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            data = (
                vehicle["accidentID"],
                int(vehicle["vehicleRef"]),
                int(vehicle["type"]),
                int(vehicle["vehicleAge"]),
                int(vehicle["vehicleIMD"]),
                int(vehicle["manoeuvre"]),
                int(vehicle["towingArticulation"]),
                int(vehicle["skiddingAndOverturning"]),
                int(vehicle["leftHandDrive"]),
                int(vehicle["engineCapacity"]),
                int(vehicle["pointOfImpact"]),
                int(vehicle["propulsionCode"]),
                int(vehicle["restrictedLane"]),
                int(vehicle["hitObjectInCarriageway"]),
                int(vehicle["hitObjectOffCarriageway"]),
                int(vehicle["leavingCarriageway"]),
            )
            cursor.execute(query, data)

        for person in people:
            query = "insert into Person values(%s,%s,%s,%s,%s,%s,%s,%s)"
            data = (
                person["accidentID"],
                int(person["vehicleRef"]),
                int(person["casualtyRef"]),
                int(person["gender"]),
                int(person["personAge"]),
                int(person["ageBand"]),
                int(person["homeType"]),
                int(person["personIMD"]),
            )
            cursor.execute(query, data)

        for driver in drivers:
            query = "insert into Driver values(%s,%s,%s,%s)"
            data = (
                driver["accidentID"],
                int(driver["vehicleRef"]),
                int(driver["casualtyRef"]),
                int(driver["journeyPurpose"])
            )
            cursor.execute(query, data)

        for casualty in casualties:
            query = "insert into Casualty values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            data = (
                casualty["accidentID"],
                int(casualty["vehicleRef"]),
                int(casualty["casualtyRef"]),
                int(casualty["casualtyClass"]),
                int(casualty["casualtySeverity"]),
                int(casualty["pedLocation"]),
                int(casualty["pedMovement"]),
                int(casualty["carPassenger"]),
                int(casualty["busPassenger"]),
                int(casualty["maintenanceWorker"]),
                int(casualty["casualtyType"]),
            )
            cursor.execute(query, data)

        cnx.commit()
        print("The accident has been successfully created!")

        querySelect = "select * from Accidents where accidentID = " +accidentID
        cursor.execute(querySelect)
        fetch = cursor.fetchone()
        display = beautifultable.BeautifulTable(maxwidth=200)
        display.columns.header = fetch.keys()
        display.rows.append(fetch.values())
        print(display)

    except Exception as e:
        if isinstance(e, ValueError):
            print("One or more of the input fields have issues. Check that the types that you are attempting to enter match the field.")
        elif isinstance(e, mysql.connector.IntegrityError):
            print("The accidentID you have entered already exists in the database.")
        else:
            print("Create ran into an unknown error.")