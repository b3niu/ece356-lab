create table acc_tmp (
    Accident_Index char(13) primary key,
    Location_Easting_OSGR int,
    Location_Northing_OSGR int,
    Longitude decimal(10,6),
    Latitude decimal(10,6),
    Police_Force int,
    Accident_Severity int,
    Number_of_Vehicles int,
    Number_of_Casualties int,
    Date char(10),
    Day_of_Week int,
    Time char(5),
    Local_Authority_District int,
    Local_Authority_Highway char(9),
    X1st_Road_Class int,
    X1st_Road_Number int,
    Road_Type int,
    Speed_limit char(2),
    Junction_Detail int,
    Junction_Control int,
    X2nd_Road_Class int,
    X2nd_Road_Number int,
    Pedestrian_CrossingHuman_Control int,
    Pedestrian_CrossingPhysical_Facilities int,
    Light_Conditions int,
    Weather_Conditions int,
    Road_Surface_Conditions int,
    Special_Conditions_at_Site int,
    Carriageway_Hazards int,
    Urban_or_Rural_Area int,
    Did_Police_Officer_Attend_Scene_of_Accident int,
    LSOA_of_Accident_Location char(9)
);

create table cas_tmp (
    Accident_Index char(13),
    Vehicle_Reference int,
    Casualty_Reference int,
    Casualty_Class int,
    Sex_of_Casualty int,
    Age_of_Casualty int,
    Age_Band_of_Casualty int,
    Casualty_Severity int,
    Pedestrian_Location int,
    Pedestrian_Movement int,
    Car_Passenger int,
    Bus_or_Coach_Passenger int,
    Pedestrian_Road_Maintenance_Worker int,
    Casualty_Type int,
    Casualty_Home_Area_Type int,
    Casualty_IMD_Decile int,
    primary key (Accident_Index, Vehicle_Reference, Casualty_Reference)
);

create table veh_tmp (
    Accident_Index char(13),
    Vehicle_Reference int,
    Vehicle_Type int,
    Towing_and_Articulation int,
    Vehicle_Manoeuvre int,
    Vehicle_LocationRestricted_Lane int,
    Junction_Location int,
    Skidding_and_Overturning int,
    Hit_Object_in_Carriageway int,
    Vehicle_Leaving_Carriageway int,
    Hit_Object_off_Carriageway int,
    X1st_Point_of_Impact int,
    Was_Vehicle_Left_Hand_Drive int,
    Journey_Purpose_of_Driver int,
    Sex_of_Driver int,
    Age_of_Driver int,
    Age_Band_of_Driver int,
    Engine_Capacity_CC int,
    Propulsion_Code int,
    Age_of_Vehicle int,
    Driver_IMD_Decile int,
    Driver_Home_Area_Type int,
    Vehicle_IMD_Decile int,
    primary key (Accident_Index, Vehicle_Reference)
);

load data infile '/var/lib/mysql-files/07-Accidents/acc2005_2016.csv' into table acc_tmp fields terminated by ',' enclosed by '"' lines terminated by '\r\n' ignore 1 rows
(Accident_Index,@vEasting,@vNorthing,@vLongitude,@vLatitude,Police_Force,Accident_Severity,Number_of_Vehicles,Number_of_Casualties,Date,Day_of_Week,Time,Local_Authority_District,Local_Authority_Highway,X1st_Road_Class,X1st_Road_Number,Road_Type,@vSpeed,Junction_Detail,Junction_Control,X2nd_Road_Class,X2nd_Road_Number,Pedestrian_CrossingHuman_Control,Pedestrian_CrossingPhysical_Facilities,Light_Conditions,Weather_Conditions,Road_Surface_Conditions,Special_Conditions_at_Site,Carriageway_Hazards,Urban_or_Rural_Area,Did_Police_Officer_Attend_Scene_of_Accident,@vLSOA)
set Location_Easting_OSGR = NULLIF(@vEasting, 'NA'),
Location_Northing_OSGR = NULLIF(@vNorthing, 'NA'),
Longitude = NULLIF(@vLongitude, 'NA'),
Latitude = NULLIF(@vLatitude, 'NA'),
Speed_limit = NULLIF(@vSpeed, 'NULL'),
LSOA_of_Accident_Location = NULLIF(@vLSOA,'');
load data infile '/var/lib/mysql-files/07-Accidents/cas2005_2016.csv' into table cas_tmp fields terminated by ',' enclosed by '"' lines terminated by '\r\n' ignore 1 rows;
load data infile '/var/lib/mysql-files/07-Accidents/veh2005_2016.csv' into table veh_tmp fields terminated by ',' enclosed by '"' lines terminated by '\r\n' ignore 1 rows;


create table Accidents (
    accidentID char(13) primary key,
    accidentSeverity int,
    num_vehicles int,
    num_casualties int,
    date DATE,
    dayOfWeek int,
    time TIME,
    policeForce int,
    policeAttendance int,
    check (dayOfWeek between 1 and 7),
    check (policeForce between 1 and 99),
    check (policeAttendance between -1 and 3)
);

create table RoadInfo (
    accidentID char(13) primary key,
    roadType int,
    firstRoadClass int,
    firstRoadNumber int,
    speedLimit char(2),
    pedCrossingHuman int,
    pedCrossingPhysical int,
    foreign key (accidentID) references Accidents(accidentID),
    check (pedCrossingHuman between -1 and 2),
    check (pedCrossingPhysical between -1 and 8),
    check (firstRoadClass between -1 and 6),
    check (roadType between -1 and 12)
);

create table Intersection (
    accidentID char(13) primary key,
    secondRoadClass int,
    secondRoadNumber int,
    junctionDetail int,
    junctionControl int,
    junctionLocation int,
    foreign key (accidentID) references RoadInfo(accidentID),
    check (junctionDetail between -1 and 9),
    check (junctionControl between -1 and 4),
    check (junctionLocation between -1 and 8),
    check (secondRoadClass between -1 and 6)
);

create table AccidentConditions (
    accidentID char(13) primary key,
    lightConditions int,
    weatherConditions int,
    roadSurfaceConditions int,
    specialConditions int,
    carriagewayHazards int,
    foreign key (accidentID) references Accidents(accidentID),
    check (lightConditions between -1 and 7)
);

create table LocationData (
    accidentID char(13) primary key,
    locationEasting int,
    locationNorthing int,
    locationLatitude decimal(10,6),
    locationLongitude decimal(10,6),
    localAuthorityDistrict int,
    localAuthorityHighway char(9),
    urbanOrRural int,
    LSOA char(9),
    foreign key (accidentID) references Accidents(accidentID),
    check (localAuthorityDistrict between 1 and 999),
    check (urbanOrRural between 1 and 3)
);

create table Vehicle (
    accidentID char(13),
    vehicleRef int,
    type int,
    vehicleAge int,
    vehicleIMD int,
    manoeuvre int,
    towingArticulation int,
    skiddingAndOverturning int,
    leftHandDrive int,
    engineCapacity int,
    pointOfImpact int,
    propulsionCode int,
    restrictedLane int,
    hitObjectInCarriageway int,
    hitObjectOffCarriageway int,
    leavingCarriageway int,
    primary key (accidentID, vehicleRef),
    foreign key (accidentID) references Accidents(accidentID),
    check (type between -1 and 98),
    check (manoeuvre between -1 and 18),
    check (restrictedLane between -1 and 10)
);

create table Person (
    accidentID char(13),
    vehicleRef int,
    casualtyRef int,
    gender int,
    personAge int,
    ageBand int,
    homeType int,
    personIMD int,
    primary key (accidentID, vehicleRef, casualtyRef),
    foreign key (accidentID, vehicleRef) references Vehicle(accidentID, vehicleRef),
    foreign key (accidentID) references Accidents(accidentID),
    check (ageBand between -1 and 11),
    check (gender between -1 and 3)
);

create table Driver (
    accidentID char(13),
    vehicleRef int,
    casualtyRef int,
    journeyPurpose int,
    primary key (accidentID, vehicleRef, casualtyRef),
    foreign key (accidentID, vehicleRef, casualtyRef) references Person(accidentID, vehicleRef, casualtyRef),
    foreign key (accidentID, vehicleRef) references Vehicle(accidentID, vehicleRef),
    foreign key (accidentID) references Accidents(accidentID),
    check (journeyPurpose between -1 and 15)
);

create table Casualty (
    accidentID char(13),
    vehicleRef int,
    casualtyRef int,
    casualtyClass int,
    casualtySeverity int,
    pedLocation int,
    pedMovement int,
    carPassenger int,
    busPassenger int,
    maintenanceWorker int,
    casualtyType int,
    primary key (accidentID, vehicleRef, casualtyRef),
    foreign key (accidentID, vehicleRef, casualtyRef) references Person(accidentID, vehicleRef, casualtyRef),
    foreign key (accidentID, vehicleRef) references Vehicle(accidentID, vehicleRef),
    foreign key (accidentID) references Accidents(accidentID),
    check (casualtySeverity between 1 and 3),
    check (casualtyClass between 1 and 3),
    check (pedLocation between -1 and 10),
    check (pedMovement between -1 and 9),
    check (casualtyType between 0 and 99)
);


insert into Accidents (accidentID,accidentSeverity,num_vehicles,num_casualties,date,dayOfWeek,time,policeForce,policeAttendance)
select Accident_Index, Accident_Severity, Number_of_Vehicles, Number_of_Casualties, str_to_date(Date, '%d/%m/%Y'), Day_of_Week, str_to_date(Time, '%H:%i'), Police_Force, Did_Police_Officer_Attend_Scene_of_Accident from acc_tmp;

insert into RoadInfo (accidentID,roadType,firstRoadClass,firstRoadNumber,speedLimit,pedCrossingHuman,pedCrossingPhysical)
select Accident_Index, Road_Type, X1st_Road_Class, X1st_Road_Number, Speed_limit, Pedestrian_CrossingHuman_Control, Pedestrian_CrossingPhysical_Facilities from acc_tmp;

insert into Intersection (accidentID,secondRoadClass,secondRoadNumber,junctionDetail,junctionControl,junctionLocation)
select Accident_Index, X2nd_Road_Class, X2nd_Road_Number, Junction_Detail, Junction_Control, Junction_Location from (acc_tmp inner join veh_tmp using (Accident_Index)) where X2nd_Road_Class != -1 and Vehicle_Reference = 1;

insert into AccidentConditions (accidentID,lightConditions,weatherConditions,roadSurfaceConditions,specialConditions,carriagewayHazards)
select Accident_Index, Light_Conditions, Weather_Conditions, Road_Surface_Conditions, Special_Conditions_at_Site, Carriageway_Hazards from acc_tmp;

insert into LocationData (accidentID,locationEasting,locationNorthing,locationLatitude,locationLongitude,localAuthorityDistrict,localAuthorityHighway,urbanOrRural,LSOA)
select Accident_Index, Location_Easting_OSGR, Location_Northing_OSGR, Latitude, Longitude, Local_Authority_District, Local_Authority_Highway, Urban_or_Rural_Area, LSOA_of_Accident_Location from acc_tmp;

insert into Vehicle (accidentID,vehicleRef,type,vehicleAge,vehicleIMD,manoeuvre,towingArticulation,skiddingAndOverturning,leftHandDrive,engineCapacity,pointOfImpact,propulsionCode,restrictedLane,hitObjectInCarriageway,hitObjectOffCarriageway,leavingCarriageway)
select Accident_Index, Vehicle_Reference, Vehicle_Type, Age_of_Vehicle, Vehicle_IMD_Decile, Vehicle_Manoeuvre, Towing_and_Articulation, Skidding_and_Overturning, Was_Vehicle_Left_Hand_Drive, Engine_Capacity_CC, X1st_Point_of_Impact, Propulsion_Code, Vehicle_LocationRestricted_Lane, Hit_Object_in_Carriageway, Hit_Object_off_Carriageway, Vehicle_Leaving_Carriageway from veh_tmp;

insert into Person (accidentID,vehicleRef,casualtyRef,gender,personAge,ageBand,homeType,personIMD)
select Accident_Index, Vehicle_Reference, Casualty_Reference, Sex_of_Casualty, Age_of_Casualty, Age_Band_of_Casualty, Casualty_Home_Area_Type, Casualty_IMD_Decile from cas_tmp;
insert into Person (accidentID,vehicleRef,casualtyRef,gender,personAge,ageBand,homeType,personIMD)
select Accident_Index, Vehicle_Reference, -1, Sex_of_Driver, Age_of_Driver, Age_Band_of_Driver, Driver_Home_Area_Type, Driver_IMD_Decile from veh_tmp where (Accident_Index, Vehicle_Reference) not in (select accidentID, vehicleRef from (Person inner join cas_tmp on Person.accidentID = cas_tmp.Accident_Index and Person.vehicleRef = cas_tmp.Vehicle_Reference) where Casualty_Class = 1);

insert into Driver (accidentID,vehicleRef,casualtyRef,journeyPurpose)
select veh_tmp.Accident_Index, veh_tmp.Vehicle_Reference, Casualty_Reference, Journey_Purpose_of_Driver from (veh_tmp inner join cas_tmp on veh_tmp.Accident_Index = cas_tmp.Accident_Index and veh_tmp.Vehicle_Reference = cas_tmp.Vehicle_Reference) where Casualty_Class = 1;
insert into Driver (accidentID,vehicleRef,casualtyRef,journeyPurpose)
select Accident_Index, Vehicle_Reference, -1, Journey_Purpose_of_Driver from veh_tmp where (Accident_Index, Vehicle_Reference) not in (select accidentID, vehicleRef from Driver);

insert into Casualty (accidentID,vehicleRef,casualtyRef,casualtyClass,casualtySeverity,pedLocation,pedMovement,carPassenger,busPassenger,maintenanceWorker,casualtyType)
select Accident_Index, Vehicle_Reference, Casualty_Reference, Casualty_Class, Casualty_Severity, Pedestrian_Location, Pedestrian_Movement, Car_Passenger, Bus_or_Coach_Passenger, Pedestrian_Road_Maintenance_Worker, Casualty_Type from cas_tmp;


drop table acc_tmp;
drop table veh_tmp;
drop table cas_tmp;