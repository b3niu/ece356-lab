import datetime
import constants
import mysql.connector
import beautifultable

def modify_accident(cnx, cursor):
    accidentID = input("What is the ID of the accident you are trying to modify: ")

    print("The available categories are " + ", ".join(constants.tables.keys()) + ".")
    table = input("Please enter the category, or 'exit' to quit: ").title()
    while table not in constants.tables:
        if table.lower() is "exit":
            exit(0)
        print("Category not found. The available categories are: " + ", ".join(constants.tables.keys()) + ".")
        table = input("Please enter the category, or 'exit' to quit: ").title()

    if table == "Vehicle":
        vehicleRef = input("What is the reference of the vehicle: ")
    elif table == "Driver":
        vehicleRef = input("What is the reference of the vehicle: ")
        casualtyRef = -1
    elif table == "Casualties":
        vehicleRef = input("What is the reference of the vehicle: ")
        casualtyRef = input("What is the reference of the casualty: ")
    print("The available fields are " + ", ".join(list(filter(lambda x: x != "Accident ID", constants.filters[table].keys()))) + ".")
    field = input("Please enter the field you would like to modify, or 'exit' to quit: ").title()
    while field not in constants.filters[table]:
        if field.lower() is "exit":
            exit(0)
        print("Field not found. The available fields are: " + " ".join(constants.filters[table].keys()) + ".")
        field = input("Please enter the field you would like to modify, or 'exit' to quit: ").title()

    value = input("What is the new value of this field: ")

    query = "select * from " + constants.tables[table] + " where accidentID = " + accidentID

    if table == "Vehicle":
        queryWhere = " and vehicleRef = " + vehicleRef
    elif table == "Driver" or table == "Casualties":
        queryWhere = " and vehicleRef = " + vehicleRef + " and casualtyRef = " + casualtyRef
    else:
        queryWhere = ""
    cursor.execute(query + queryWhere)
    old = cursor.fetchone()
    display = beautifultable.BeautifulTable(maxwidth=200)
    display.columns.header = old.keys()
    display.rows.append(old.values())
    new = list(old.values())
    for colIndex in range(len(old.keys())):
        if list(old.keys())[colIndex] == constants.filters[table][field]:
            new[colIndex] = value
            break
    display.rows.append(new)
    display.rows.header = ["old", "new"]
    print(display)
    prompt = input("Would you like to continue this modification? (y/n): ").lower()
    while prompt not in ["y", "yes", "n", "no"]:
        prompt = input("Invalid response. Would you like to continue this modification? (y/n): ").lower()
    if prompt == "y" or prompt == "yes":
        try:
            query = "update " + constants.tables[table] + " set " + constants.filters[table][field] + " = " + value + " where accidentID = '" + accidentID + "'"
            cursor.execute(query + queryWhere)
            cnx.commit()
            print("Successfully modified")
        except Exception as e:
            print("An error occurred while trying to modify")
    else:
        print("Canceled modification")
