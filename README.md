# Setup
To set up our database and client application, a few prerequisites are needed. These are also listed in the Setup section of the Project Report.
1. Python 3 (preferably Python 3.7)
2. pip or an equivalent Python package manager
    - pip install mysql
    - pip install beautifultable
3. MySQL
4. UK Traffic Accident CSV data from Kaggle (https://www.kaggle.com/silicon99/dft-accident-data)
5. Our client application and SQL code (You are here! Link is https://git.uwaterloo.ca/b3niu/ece356-lab)

For our testing, a local deployment of MySQL was used, and the CSVs used were downloaded on Kaggle rather than the ones on Marmoset. We have changed the file paths to reflect Marmoset. However, the following should be changed regardless during staff testing:
1. The file paths located in lines 83 to 92 in create_tables.sql
2. The mysql-connector database connection string in main.py

After the above prerequisites and steps are done, the following steps are for setup:
1. Adjust MySQL Timeout and Memory parameters accordingly.
2. Run create_tables.sql
3. Run create_indexes.sql
4. Execute python3 main.py
