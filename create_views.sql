create view PersonDriver as 
    select * from 
        (Person inner join Driver using (accidentID, vehicleRef, casualtyRef));

create view PersonCasualty as   
    select * from
        (Person inner join Casualty using (accidentID, vehicleRef, casualtyRef));