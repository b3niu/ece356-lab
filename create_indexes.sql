create index date_index on Accidents(date);
create index speed_index on RoadInfo(speedLimit);
create index district_index on LocationData(localAuthorityDistrict);
create index highway_index on LocationData(localAuthorityHighway);
create index vtype_index on Vehicle(type);
create index age_index on Person(personAge);
create index severity_index on Casualty(casualtySeverity);