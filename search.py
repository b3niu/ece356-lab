import datetime
import constants
import mysql.connector
import beautifultable

# REFERENCES:
# https://dev.mysql.com/doc/connector-python/en/connector-python-example-cursor-select.html
# https://dev.mysql.com/doc/connector-python/en/connector-python-api-mysqlcursor-column-names.html
# https://dev.mysql.com/doc/connector-python/en/connector-python-api-mysqlcursor-fetchall.html

def construct_query(cnx, cursor):
    # What columns to return, a.k.a what table to get data from?
    print("What information pertaining to the accident would you like to see? The available categories are " + ", ".join(constants.tables.keys()) + ".")
    cat = input("Please enter the category, or 'back' to return to operation selection: ").title()
    while cat not in constants.tables:
        if cat.lower() == "back":
            cursor.close()
            return
        print("Category not found. The available categories are: " + ", ".join(constants.tables.keys()) + ".")
        cat = input("Please enter the category, or 'back' to return to operation selection: ").title()

    query = "SELECT * FROM " + constants.tables[cat]
    where_clause = " WHERE "

    # What conditions to filter from? This determines what tables to inner join, as well as the WHERE query.
    done = False
    joined = {cat}
    while not done:
        print("How would you like to filter the data? The filter categories are: " + ", ".join(constants.tables.keys()) + ".")
        filter_cat = input("Please enter the filter category, or 'cancel' for no filter: ").title()
        while filter_cat not in constants.tables:
            print("Filter category not found. The filter categories are: " + ", ".join(constants.tables.keys()) + ".")
            filter_cat = input("Please enter the filter category, or 'cancel' for no filter: ").title()
            if filter_cat.lower() == "cancel":
                break
        if filter_cat.lower() == "cancel":
            done = True
            break

        print("The filters available for " + filter_cat + " are: " + ", ".join(constants.filters[filter_cat].keys()))
        filt = input("Please enter the desired filter: ").title()
        while filt not in constants.filters[filter_cat]:
            print("Filter not found. The filters available for " + filter_cat + " are: " + ", ".join(constants.filters[filter_cat].keys()))
            filt = input("Please enter the desired filter: ").title()

        # TODO: implement logic to allow for range filtering (eg. age between x and y) and other operations. Somehow keep track of valid operations for each filter?
        # Alternatively, give option for exact match, partial match for strings / approximate match for numbers earlier

        # TODO: input validation? would have to keep track of data types... maybe add something in constants for data type of each field
        comparator = input("The available comparators are <, > , =. Please input a comparator: ")
        while comparator not in ["<", ">", "="]:
            comparator = input("Invalid input. The available comparators are <, > , =. Please input a comparator: ")
        val = input("Please enter the desired value for '" + filt + " " + comparator + "'")
        while not val:
            val = input("Invalid input. Please enter the desired value for '" + filt + " " + comparator + "'")

        if filter_cat not in joined:
            joined.add(filter_cat)
            query += " INNER JOIN " + constants.tables[filter_cat] + " USING (accidentID)"
        
        if where_clause.rstrip().split(" ")[-1] != "WHERE":
            where_clause += " AND "
        
        if type(filt) is tuple:
            where_clause += "(" + " OR ".join([f + " = " + val for f in filt]) + ")"
        else:
            where_clause += "(" + constants.filters[filter_cat][filt] + " " + comparator + " " + val

        prompt = input("Would you like to add an OR clause to this filter? (y/n): ")
        while prompt not in ["y", "yes", "n", "no"]:
            prompt = input("Invalid response. Would you like to add an OR clause to this filter (" + filt + ")? (y/n): ").lower()
        while prompt == "y" or prompt == "yes":
            comparator = input("The available comparators are <, > , =. Please input a comparator: ")
            while comparator not in ["<", ">", "="]:
                comparator = input("Invalid input. The available comparators are <, > , =. Please input a comparator: ")
            val = input("Please enter the desired value for '" + filt + " " + comparator + "'")
            while not val:
                val = input("Invalid input. Please enter the desired value for '" + filt + " " + comparator + "'")
            where_clause += " OR " + constants.filters[filter_cat][filt] + " " + comparator + " " + val
            prompt = input("Would you like to add another OR clause to the filter? (y/n): ")
            while prompt not in ["y", "yes", "n", "no"]:
                prompt = input("Invalid response. Would you like to add another OR clause to the filter (" + filt + ")? (y/n): ").lower()
        
        where_clause += ")"
        
        prompt = input("Would you like to add more filters? (y/n): ").lower()
        while prompt not in ["y", "yes", "n", "no"]:
            prompt = input("Invalid response. Would you like to add more filters? (y/n): ").lower()
        if prompt == "n" or prompt == "no":
            done = True

    cursor.execute(query.replace("*", "count(*)") + where_clause + ";")
    fetchCount = cursor.fetchone()['count(*)']

    cursor.execute(query + where_clause + ";")
    display = beautifultable.BeautifulTable(maxwidth=200)
 
    display_count = input("Your query contains " + str(fetchCount) + " rows. Please enter how many rows would you like to display: ")
    while not display_count.isnumeric():
        display_count = input("Invalid input. Please enter how many rows would you like to display: ")

    display.columns.header = cursor.column_names
    fetch = cursor.fetchmany(size=int(display_count))
    for fetched in fetch:
        display.rows.append(fetched.values())

    print(display)

    remaining_rows = fetchCount - cursor.rowcount
    while remaining_rows > 0:
        if remaining_rows < int(display_count):
            prompt = input("There are " + str(remaining_rows) + " rows remaining. Would you like to view the remaining rows (y/n)? ").lower()
        else:
            prompt = input("There are " + str(remaining_rows) + " rows remaining. Would you like to view the next " + display_count + " rows (y/n)? ").lower()
        while prompt not in ["y", "yes", "n", "no"]:
            prompt = input("Invalid response. Would you like to view the next " + display_count + " rows (y/n)? ").lower()
        if prompt == "y" or prompt == "yes":
            display.clear()
            fetch = cursor.fetchmany(size=int(display_count))
            for fetched in fetch:
                display.rows.append(fetched.values())
            remaining_rows = fetchCount - cursor.rowcount
            print(display)
        else:
            break
        