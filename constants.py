# Maps user-friendly name to relevant table.
tables = {
    "Road": "RoadInfo", 
    "Intersection": "Intersection",
    "Conditions": "AccidentConditions", 
    "Location": "LocationData", 
    "Vehicle": "Vehicle",
    "Casualties": "PersonCasualty",
    "Driver": "PersonDriver",
    "Accident Details": "Accidents"
}

# Filters and their corresponding fields to query.
# Tuples are used to query against both fields.
# TODO: add more filters, these were just skeleton
filters = {
    "Road": {
        "Road Type": "roadType", 
        "Speed Limit": "speedLimit",
        "Road Number": "firstRoadNumber",
        "Road Class": "firstRoadClass",
        "Pedestrian Crossing Control": "pedCrossingHuman",
        "Pedestrian Crossing Type": "pedCrossingPhysical",
    },
    "Intersection": {
        "Second Road Number": "secondRoadNumber",
        "Second Road Class": "secondRoadClass",
        "Junction Detail": "junctionDetail",
        "Junction Control": "junctionControl",
        "Junction Location": "junctionLocation"
    },
    "Conditions": {
        "Light": "lightConditions",
        "Weather": "weatherConditions",
        "Road Surface": "roadSurfaceConditions",
        "Carriageway Hazards": "carriagewayHazards",
        "Special Conditions": "specialConditions"
    }, 
    "Location": {
        "Easting": "locationEasting",
        "Northing": "locationNorthing",
        "Latitude": "locationLatitude",
        "Longitude": "locationLongitude",
        "Local Authority District": "localAuthorityDistrict",
        "Local Authority Highway": "localAuthorityHighway",
        "Urban or Rural": "urbanOrRural",
        "LSOA": "LSOA"
    }, 
    "Vehicle": {
        "Type": "type",
        "Age": "vehicleAge",
        "IMD Decile": "vehicleIMD",
        "Manoeuvre": "manoeuvre",
        "Towing & Articulation": "towingArticulation",
        "Skidding & Overturning": "skiddingAndOverturning",
        "Left Hand Drive": "leftHandDrive",
        "Engine Capacity": "engineCapacity",
        "Point of Impact": "pointOfImpact",
        "Propulsion Code": "propulsionCode",
        "Restricted Lane": "restrictedLane",
        "Hit Object in Carriageway": "hitObjectInCarriageway",
        "Hit Object off Carriageway": "hitObjectOffCarriageway",
        "Leaving Carriageway": "leavingCarriageway"
    },
    "Casualties": {
        "Age": "personAge",
        "Age Band": "ageBand",
        "Gender": "gender",
        "Class": "casualtyClass",
        "Type": "casualtyType",
        "Home Type": "homeType",
        "IMD Decile": "personIMD",
        "Severity": "casualtySeverity",
        "Pedestrian Location": "pedLocation",
        "Pedestrian Movement": "pedMovement",
        "Car Passenger": "carPassenger",
        "Bus Passenger": "busPassenger",
        "maintenanceWorker": "maintenanceWorker"
    },
    "Driver": {
        "Age": "personAge",
        "Age Band": "ageBand",
        "Gender": "gender",
        "Home Type": "homeType",
        "IMD Decile": "personIMD",
        "Journey Purpose": "journeyPurpose"
    },
    "Accident Details": {
        "Accident ID": "accidentID",
        "Accident Severity": "accidentSeverity",
        "Number of Vehicles": "num_vehicles",
        "Number of Casualties": "num_casualties",
        "Date": "date",
        "Day Of the Week": "dayOfWeek",
        "Time": "time",
        "Police Force": "policeForce",
        "Police Attendance": "policeAttendance"
    }
}

create = {
    "Person": {
        "Casualty Reference": "casualtyRef",
        "Gender": "gender",
        "Age": "personAge",
        "Age Band": "ageBand",
        "Home Type": "homeType",
        "IMD Decile": "personIMD",
    },
    "Casualties": {
        "Class": "casualtyClass",
        "Type": "casualtyType",
        "Severity": "casualtySeverity",
        "Pedestrian Location": "pedLocation",
        "Pedestrian Movement": "pedMovement",
        "Car Passenger": "carPassenger",
        "Bus Passenger": "busPassenger",
        "Maintenance Worker": "maintenanceWorker"
    },
    "Driver": {
        "Journey Purpose": "journeyPurpose"
    }
}
